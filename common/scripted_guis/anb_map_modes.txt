﻿quest_map_mode_scripted_gui = {
	effect = {
		if = {
			limit = {
				OR = {
					NOT = { exists = global_var:quest_map_mode }
					NOT = { global_var:quest_map_mode = flag:quest_map_mode }
				}
			}
			set_global_variable = {
				name = quest_map_mode_changing
				value = 1
			}
			set_global_variable = {
				name = quest_map_mode
				value = flag:quest_map_mode
			}
			every_barony = {
				if = {
					limit = {
						county = {
							has_quest = yes
						}
					}
					set_color_from_title = title:d_map_color_dark_red
					
					if = {
						limit = {
							is_capital_barony = yes
						}
						set_color_from_title = title:d_map_color_red # Colour capital baronies (the place you get quests) so they're easier to see
					}
				}
				else = {
					set_color_from_title = title:d_map_color_green
				}
			}
			remove_global_variable = quest_map_mode_changing
		}
	}
	is_shown = {
		exists = global_var:quest_map_mode
		global_var:quest_map_mode = flag:quest_map_mode
	}
	is_valid = {
		NOT = { exists = global_var:quest_map_mode_changing }
	}
}