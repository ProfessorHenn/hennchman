﻿# Arda of Nurael
40000 = {
	name = "Arda"
	#dna = 40000_duke_arda
	dynasty = dynasty_nurael
	religion = elven_forebears
	culture = moon_elvish
	female = yes

	trait = content
	trait = compassionate
	trait = diligent
	trait = education_learning_2
	trait = elven_purist
	trait = lifestyle_physician
	
	814.4.18 = {
		birth = yes
	}
	
	910.1.1 = {
		effect = {
			set_relation_friend = character:75	#Ibenion
			
			add_trait_xp = {
				trait = lifestyle_physician
				value = 100
			}
		}
	}
}

# Ultarion of Larthan
40001 = {
	name = "Ultarion"
	#dna = 40001_duke_ultarion
	dynasty = dynasty_larthan
	religion = elven_forebears
	culture = moon_elvish

	trait = greedy
	trait = fickle
	trait = temperate
	trait = education_diplomacy_3
	
	735.12.1 = {
		birth = yes
	}
	
	910.1.1 = {
		effect = {
			set_relation_friend = character:75	#Ibenion
		}
	}
}

# Carodir of Larthan
40003 = {
	name = "Carodir"
	#dna = 40003_carodir
	dynasty = dynasty_silcarod
	religion = elven_forebears
	culture = moon_elvish

	trait = stubborn
	trait = wrathful
	trait = zealous
	trait = education_martial_2
	
	785.3.3 = {
		birth = yes
	}
}

#Camnaril of Venail
40004 = {
	name = "Camnaril"
	dynasty = dynasty_truesight
	religion = elven_forebears
	culture = moon_elvish
	
	trait = shy
	trait = greedy
	trait = honest
	trait = education_diplomacy_2
	
	837.7.13 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
	1001.1.1 = {
		effect = {
			set_relation_friend = character:40005	#Liandel
		}
	}
}

#Liandel of Edilliande
40005 = {
	name = "Liandel"
	dynasty = dynasty_seawatcher
	religion = elven_forebears
	culture = moon_elvish
	female = yes
	
	trait = deceitful
	trait = ambitious
	trait = zealous
	trait = beauty_good_2
	trait = education_intrigue_3
	
	880.6.21 = {
		birth = yes
	}
	
	1004.1.1 = {
		add_matrilineal_spouse = 40006 #Finorian
	}
	
	1021.10.31 = {
		effect = {
			add_hook_no_toast = {
				type = favor_hook
				target = character:40004 #Camnaril
			}
		}
	}
}

# Spouse of Liandel
40006 = {
	name = "Finorian"
	religion = elven_forebears
	culture = moon_elvish
	
	trait = trusting
	trait = lustful
	trait = content
	trait = education_stewardship_1
	
	848.3.18 = {
		birth = yes
	}
	
	1004.1.1 = {
		add_matrilineal_spouse = 40005	#Liandel
	}
}