k_bjarnrik = {
	1000.1.1 = { change_development_level = 6 }
	999.2.3 = {
		holder = 300005
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300000 #Gunnar Bjarnsson
		government = tribal_government
	}
}

d_bjarnland = {
	1000.1.1 = { change_development_level = 7 }
	999.2.3 = {
		holder = 300005
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300000 #Gunnar Bjarnsson
		government = tribal_government
	}
}

c_konugrhavn = {
	1000.1.1 = { change_development_level = 10 }
}

d_bifrutja = {
	1000.1.1 = { change_development_level = 7 }
	999.2.3 = {
		holder = 300005
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300000 #Gunnar Bjarnsson
		government = tribal_government
	}
}

d_revrland = {
	1000.1.1 = { change_development_level = 7 }
	999.2.3 = {
		liege = k_bjarnrik
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300003 #Einar Reaver
		government = tribal_government
	}
}
c_fuglborg = {
	999.2.3 = {
		liege = k_bjarnrik
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300003 #Einar Reaver
		government = tribal_government
		liege = d_revrland
	}
}

c_revrhavn = {
	1000.1.1 = { change_development_level = 9 }
}

c_alptborg = {
	1000.1.1 = { change_development_level = 7 }
	999.2.3 = {
		liege = k_bjarnrik
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300002 #Skali Sidaett
		government = tribal_government
		liege = d_bifrutja
	}
}

c_fegras = {
	1000.1.1 = { change_development_level = 7 }
	999.2.3 = {
		holder = 300005
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300000 #Gunnar Bjarnsson
		government = tribal_government
	}
}
d_sidaett = {
	999.2.3 = {
		liege = k_bjarnrik
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300002 #Skali Sidaett
		government = tribal_government
	}
}

c_sidaett = {
	1000.1.1 = { change_development_level = 8 }
}

d_ismark = {
	1000.1.1 = { change_development_level = 5 }
	999.2.3 = {
		liege = k_bjarnrik
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300002 #Skali Sidaett
		government = tribal_government
	}
}

d_haugrmyrr = {
	1020.10.31 = {
		holder = 300008
		government = tribal_government
	}
}

c_kaldrland = {
	1020.10.31 = {
		holder = 300009
		government = tribal_government
	}
}

c_elgrbeiting = {
	1020.10.31 = {
		holder = 300010
		government = tribal_government
	}
}

d_sarbann = {
	999.2.3 = {
		holder = 300005
		government = tribal_government
	}
	1020.10.31 = {
		holder = 300011
		government = tribal_government
	}
}

c_sarbann = {
	1020.10.31 = {
		holder = 300011
		government = tribal_government
	}
}