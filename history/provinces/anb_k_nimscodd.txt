#k_nimscodd
##d_nimscodd
###c_nimscodd
169 = {		#Nimscodd

    # Misc
    culture = cliff_gnomish
    religion = the_thought
	holding = city_holding

    # History
}
1399 = {

    # Misc
    holding = none

    # History

}
1400 = {

    # Misc
    holding = none

    # History

}

###c_baycodds
146 = {		#Baycodds

    # Misc
    culture = cliff_gnomish
    religion = the_thought
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = baycodds_mines_01
		special_building = baycodds_mines_01
	}
}
1398 = {

    # Misc
    holding = none

    # History

}

##d_storm_isles
###c_storm_isles
190 = {		#Norcodd

    # Misc
    culture = cliff_gnomish
    religion = the_thought
	holding = castle_holding

    # History
}

###c_lilcodd
188 = {		#Lilcodd

    # Misc
    culture = cliff_gnomish
    religion = the_thought
	holding = castle_holding

    # History
}

###c_oddcodd
198 = {		#Oddcodd

    # Misc
    culture = cliff_gnomish
    religion = the_thought
	holding = castle_holding

    # History
}
199 = {		#Docodd

    # Misc
    holding = none

    # History
}
