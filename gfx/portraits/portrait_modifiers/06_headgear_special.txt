﻿headgear_special = {

	usage = game
	selection_behavior = weighted_random
	priority = 6


	no_hat = {
		usage = game
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = no_headgear
				value = 0
			}
		}
		outfit_tags = { no_clothes nightgown }
		weight = {
			base = 0
			modifier = {
				add = 10000
				portrait_wear_no_headgear_trigger = yes
			}
			#Anbennar
			modifier = {	#Elves tend to not wear headgear (we need to be more precise about this in the long-term after initial release, so elves can wear crowns and circlets but not helmets). Note hats above is for crowns
				add = 50
				portrait_elvish_clothing_trigger = yes
			}
		}
	}

}